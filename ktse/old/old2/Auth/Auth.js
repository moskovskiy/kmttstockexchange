import {useParams} from "old/old2/Auth/node_modules/react-router-dom";

function Auth(props) {
    let token = props.match.params.token
    localStorage.setItem('auth-token', token)
    document.location.href="/stocks"
    
    return (
        <>
            Авторизация успешна, token={localStorage.getItem('auth-token')}
        </>
    );
}

export default Auth;
