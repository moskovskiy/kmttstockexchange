import './ticker.css';
import logo from '../../media/logo-wide.png'
import axios from 'axios';
import './ticker.css'
import React, { useState, useEffect } from 'react';
import Trade from '../trade/trade'
import { Route } from 'react-router-dom'

import { Line } from 'react-chartjs-2'
let color = '#494fe6'

const options = {
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
            gridLines: {
                drawOnChartArea: false
            }
        }],
        yAxes: [
        {
            ticks: {
            beginAtZero: false,
            },
            gridLines: {
                drawOnChartArea: false
            },
            markings: {
                drawOnChartArea: false
            }
        },
        ],
    },
}

const LineChart = (props) => (
  <div className="ticker-graph">
    <Line data={props.data} options={options} />
  </div>
)

function getStock(err) {
    //alert(JSON.stringify(err))
    return {
        ticker: "ошибка",
        name: "не может быть загружен",
        platform: "ошибка " + String(err),
        price: 0,
        dayprice: 0,
        monthprice: 0,
        stocks_count: 0,
        buy: 0,
        ipo_date: 0,
        posts: 0,
        comments: 0,
        karma: 0,
        subscribers: 0
    }
}


function Ticker(props) {
    let stock = getStock(props)

    let [stonk, loadStonk] = useState({})

    //alert(JSON.stringify(props.match.params.id))

    useEffect(()=>{
        axios.get('https://api.stonks.xyz/api/v1/stock/info/' + props.match.params.id,
        {
        headers: { 'Authorization': localStorage.getItem('auth-token'),
        'Content-Type' : 'text/plain' }
        }).then((response) => {
            //alert(JSON.stringify(response))
            loadStonk(response.data.stock)
            color = stonk.color 
            //alert(JSON.stringify(response.data.stock))
        })
        .catch(function (error) {
        //alert("Нет доступа к серверу. Жаль. Error: " + JSON.stringify(error))
        loadStonk(getStock(JSON.stringify(error.message)))
        })
    }, [stonk])

    let unix_timestamp = stonk.ipo_date
    var date = new Date(unix_timestamp * 1000)
    var day = date.getDate()
    var month =  1 + date.getMonth()
    var year = date.getFullYear()

    // Will display time in 10:30:23 format
    var formattedTime = day + '.' + month + '.' + year


    const data = {
        labels: ['10.12', '11.12', '12.12', '13.12', '14.12', '15.12', '16.12', '17.12', '18.12'],
        datasets: [
        {
            label: 'Цена акции',
            data: [12.50, 14, 12.75, 12.50, 14, 12.75, 14, 14, 15.00],
            fill: false,
            backgroundColor: stonk.color,
            borderColor: stonk.color + '22',
        },
        ],
    }

    return (
        <div className="View"> 
            <Route path="/${match.path}/trade" component={Trade}/>
            <LineChart data={data}/>

            <div className="ticker-symbols">
            <div className="ticker-ticker">{stonk.ticker}</div>
            <div className="ticker-name">Пользователь {stonk.name} на {stonk.platform}</div>
            <div style={{color: stonk.color}} className="ticker-price">¢{stock.price.toFixed(2)}</div>
            </div>

            <div className="ticker-sell">
                <button onClick={()=>document.location.href="/trade/imqq"} className="ticker-action">Купить или продать</button>
                
                <br/>
                <div className="ticker-description">В портфеле 10 акций на ¢150</div>

                <br/>
                <table style={{float: 'right'}}><tr>
                <td className="ticker-block">
                    <div className="ticker-label">Цена продажи</div>
                    <div className="ticker-value">¢{stock.price.toFixed(2)}</div>
                </td>

                <td className="ticker-block">
                    <div className="ticker-label">Цена покупки</div>
                    <div className="ticker-value">¢{stock.buy.toFixed(2)}</div>
                </td>
                </tr><tr>
                <td className="ticker-block">
                    <div className="ticker-label">Дата IPO</div>
                    <div className="ticker-value">{formattedTime}</div>
                </td>

                <td className="ticker-block">
                    <div className="ticker-label">Акций в обороте</div>
                    <div className="ticker-value">{stonk.stocks_count}</div>
                </td>

                </tr>
                <tr>
                <td className="ticker-block">
                    <div className="ticker-label">Цена на начало дня</div>
                    <div className="ticker-value">¢{stock.dayprice.toFixed(2)} ({((stock.price - stock.dayprice)/(stock.dayprice)).toFixed(2)*100}%)</div>
                </td>

                <td className="ticker-block">
                    <div className="ticker-label">Цена на начало месяца</div>
                    <div className="ticker-value">¢{stock.monthprice.toFixed(2)} ({((stock.price - stock.monthprice)/(stock.monthprice)).toFixed(2)*100}%)</div>
                </td>

                </tr>
                </table>
            </div>

            <div className="ticker-info">

                <div className="ticker-tab">Информация о пользователе</div>
                <div className="ticker-block">
                    <div className="ticker-label">Карма</div>
                    <div className="ticker-value">{stonk.karma}</div>
                </div>

                <div className="ticker-block">
                    <div className="ticker-label">Постов</div>
                    <div className="ticker-value">{stonk.posts}</div>
                </div>

                <div className="ticker-block">
                    <div className="ticker-label">Комментариев</div>
                    <div className="ticker-value">{stonk.comments}</div>
                </div>

                <div className="ticker-block">
                    <div className="ticker-label">Подписчиков</div>
                    <div className="ticker-value">{stonk.subscribers}</div>
                </div>

                <div className="ticker-tab">Обсуждение в комментариях β</div>
                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>

                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>

                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>
                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>
                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>
                <a href="https://tjournal.ru" className="ticker-message">
                    <img className="ticker-message-icon" src={"https://leonardo.osnova.io/09b1a673-2a7f-5da4-aeb4-61f8cc2ffbc9/"}/>    
                    <div className="ticker-message-header">
                        serguun42 / TJournal
                    </div>
                    
                    <div className="ticker-message-text">
                        На этой неделе я бы посоветовал присмотреться к бумагам <b>$IMQQ</b>. Считаю их очень перспективными, так как автор активизировался и пишет много классных лонгридов
                    </div>
                </a>
            </div>
        </div>
    );
}

export default Ticker;
