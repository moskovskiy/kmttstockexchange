import 'old/old2/ticker/dashboard/dashboard.css';
import axios from 'old/old2/ticker/dashboard/node_modules/axios';

function GetTickers() {
    axios.get('https://api.stonks.xyz/api/v1/stocks/')
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
        alert("Нет доступа к серверу. Жаль. Error: " + JSON.stringify(error.message))
    });

    const demo = [
        {symbol:"IMQQ", name:"Пока не работает", buy: 1.00, sell: 1.00, trend: 0.20, stocks_count: 1},/*
        {symbol:"MSKY", name:"Московский", buy: 1.00, sell: 1.00, trend: -0.50, stocks_count: 1},
        {symbol:"POLD", name:"Дитковский", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},
        {symbol:"DYNA", name:"Господин Дыня", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},
        {symbol:"QQQQ", name:"qq", buy: 1.00, sell: 1.00, trend: 0.20, stocks_count: 1},
        {symbol:"MSKY", name:"Московский", buy: 1.00, sell: 1.00, trend: -0.50, stocks_count: 1},
        {symbol:"POLD", name:"Дитковский", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},
        {symbol:"DYNA", name:"Господин Дыня", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},
        {symbol:"QQQQ", name:"qq", buy: 1.00, sell: 1.00, trend: 0.20, stocks_count: 1},
        {symbol:"MSKY", name:"Московский", buy: 1.00, sell: 1.00, trend: -0.50, stocks_count: 1},
        {symbol:"POLD", name:"Дитковский", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},
        {symbol:"DYNA", name:"Господин Дыня", buy: 1.00, sell: 1.00, trend: 0.50, stocks_count: 1},*/
    ];

    const listTickers = demo.map((el, index, _) =>
            <tr className="dashboard--table--row">
                <td className="dashboard--table--row--cell--symbol dashboard--table--row--cell__left">{el.symbol}</td>
                <td className="dashboard--table--row--cell--name">{el.name}</td>
                <td className="dashboard--table--row--cell--value">{el.buy}</td>
                <td className="dashboard--table--row--cell--value">{el.sell}</td>
                {(el.trend >= 0) &&
                    <td className="dashboard--table--row--cell--trend color--green">
                        <span className="dashboard--table--row--cell--trend--arrow">+{Math.abs(el.trend)} ({Math.abs(el.trend)/el.sell}%)</span>
                    </td>
                }

                {(el.trend < 0) &&
                    <td className="dashboard--table--row--cell--trend color--red">
                        <span className="dashboard--table--row--cell--trend--arrow">-{Math.abs(el.trend)} ({Math.abs(el.trend)/el.sell}%)</span>
                    </td>
                }

                <td className="dashboard--table--row--cell--value">{el.stocks_count}</td>
                <td className="dashboard--table--row--cell--value">
                    <button onClick={()=>{
                        document.location.href='/stocks/imqq'
                    }} className="dashboard--table--row--cell--button">Профиль</button>    
                    <button onClick={()=>{}} className="dashboard--table--row--cell--button">Торговать 💰</button>    
                </td>
            </tr>
    );

    return (<>
        <table className="dashboard--table">
            <div className="dashboard--table--name">📈  Текущие торги на бирже</div>
            <tr className="dashboard--table--hrow">
                <th className="dashboard--table--header">Тикер</th>
                <th className="dashboard--table--header">Имя пользователя</th>
                <th className="dashboard--table--header">Покупка</th>
                <th className="dashboard--table--header">Продажа</th>
                <th className="dashboard--table--header">Доходность за 30 дней</th>
                <th className="dashboard--table--header">Количество акций</th>
                <th></th>
            </tr>
            {listTickers}
        </table>
    </>);
}


function Landing() {

    return (
        <> 
            <div className="dashboard--header">Листинг пользователей с акциями в обращении</div>
            <div className="dashboard--add">🤝 Заключайте сделки с другими пользователями и соберите портфель акций из крупнейших авторов</div>
            <GetTickers/>
        </>
    );
}

export default Landing;
