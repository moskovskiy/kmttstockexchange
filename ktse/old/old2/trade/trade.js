import React from 'old/old2/trade/node_modules/react'
import bg from '../../media/bg.png'
import logo from '../../media/logo.png'
import 'old/old2/trade/trade.css'

function Trade(props) {
  return (
    <div className="TradeView">
        <div className="trade-header">Купить или продать акции IMQQ</div>
        <div className="trade-box">
        </div>

        <div className="trade-glass">
            <table className="trade-table">
                <th><td className="trade-head">Цена</td><td className="trade-head">Количество</td><td className="trade-head">Сумма</td></th>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
                <tr><td className="trade-row">A</td><td className="trade-row">A</td><td className="trade-row">A</td></tr>
            </table>
        </div>
    </div>
    );
}

export default Trade;
