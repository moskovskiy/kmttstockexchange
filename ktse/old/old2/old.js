import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Landing from './landing/landing'
import Auth from './Auth/Auth'
import Dashboard from '../dashboard/dashboard'
import Menu from './menu/menu'
import Ticker from './ticker/ticker'

function AppRouter() {
  return (
    <div className="App">
      <Router>
        <Menu/>
        <Route exact path="/" component={Landing}/>
        <Route path="/auth/:token" component={Auth}/>
        <Route exact path="/stocks/" component={Dashboard}/>
        <Route path="/stocks/:id" component={Ticker}/>
        <Route path="/users/" component={Dashboard}/>
      </Router>
      <div className="bottom">
        ©2020 Stonks Exchange. Веб-приложение на платформе <a href="https://pitek.ru">Pitek</a>. Сервер биржи написал <a href="https://anisimov.io">qq</a>. Следите за обновлениями: <a href="https://dtf.ru/s/stonks">Официальный подсайт на DTF</a>, <a href="https://the.tj/s/stonks">Официальный подсайт на TJ</a>
      </div>
    </div>
  );
}

export default AppRouter;
