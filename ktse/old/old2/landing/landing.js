import 'old/old2/landing/landing.css';
import bg from '../../media/bg.png'
import logo from '../../media/logo.png'

function Landing() {
  return (
    <div className="landing">
        <img className="landing--bg" src={bg}/>
        <div className="landing--hello">Вкладывайтесь в таланты</div>
        <div className="landing--description">Инвестируйте коины в акции популярных авторов TJ и DTF и зарабатывайте состояние. Чтобы войти, отправьте '/auth' боту на вашей любимой платформе</div>

        <div className="landing--buttons">
            <button onClick={()=>document.location.href="https://tjournal.ru/m/341235"} className="landing--button landing--button--tj">Войти через TJ</button>
            <button onClick={()=>document.location.href="https://dtf.ru/m/284903"} className="landing--button landing--button--dtf">Войти через DTF</button>
        </div>
    </div>
    );
}

export default Landing;
