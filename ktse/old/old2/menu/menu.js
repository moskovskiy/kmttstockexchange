import React from 'react'
import logo from '../../media/logo-wide.png'
import { NavLink } from 'react-router-dom'
import './menu.css'

export default menu

function menu () {
    return (
      <div className="menu">
        <NavLink to="/"> <img className="dashboard--logo" src={logo}/> </NavLink>
        <div className="menu-links">
        
          <input type="text" className="menu-search" placeholder="Cтраница пользователя"/>
          <button className="menu-action">Открыть</button>
          
          <NavLink activeClassName="open-link" style={{ textDecoration: 'none' }} to="/stocks"> <div className="menu-link">Тикеры</div> </NavLink>
          <NavLink activeClassName="open-link" style={{ textDecoration: 'none' }} to="/users"> <div className="menu-link">Инвесторы</div> </NavLink>
          <NavLink activeClassName="open-link" style={{ textDecoration: 'none' }} to="/trade"> <div className="menu-link">Мои заявки</div> </NavLink>
          
          <NavLink className="right-link" style={{ textDecoration: 'none' }} to="/users/imqq"> <div className="menu-link menu-link-right">Мой пользователь</div> </NavLink>

          <span className="menu-balance">Баланс: ¢1200</span>
        </div>
      </div>
    )
}