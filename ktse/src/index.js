import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './font/index.css'
import App from './views';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals()
if ("serviceWorker" in navigator) navigator.serviceWorker.register("/sw.js", { scope: "/" });


document.addEventListener("wheel", function(event){
  if(document.activeElement.type === "number"){
      document.activeElement.blur();
  }
});
