import React from 'react'
import icon from '../../media/icon.png'
import logo from '../../media/moskovskiy.png'

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"

import './footer.css'
import Divider from '../divider/divider'

function Footer(props) {
return (<>
    <div className="footer">
        <img className="footer__icon" src={icon}/>

        <div className="footer__column">
            <span className="footer__tabs">
                <span style={{color: "#aaa"}} className="footer__tabs__tab">©2020. Помните, это лишь игра</span>
            </span>
            <span className="footer__tabs">
                <a style={{textDecoration: "none"}} href="https://t.me/stonksxyz" className="footer__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Официальный чат биржи в Telegram</a>
            </span>
        </div>

        <div className="footer__column">
            <span className="footer__tabs">
                <span style={{color: "#aaa"}} className="footer__tabs__tab">Cледите за новостями в блогах</span>
            </span>
            <span className="footer__tabs">
                <a style={{textDecoration: "none"}} href="https://tjournal.ru/s/stonks" className="footer__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Блог биржи на TJournal</a>
                <a style={{textDecoration: "none"}} href="https://dtf.ru/s/stonks" className="footer__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Блог биржи на DTF</a>
            </span>
        </div>

        <div className="footer__column">
            <span className="footer__tabs">
                <span style={{color: "#aaa"}} className="footer__tabs__tab">Автор биржи: QQ</span>
            </span>
            <span className="footer__tabs">
                <a style={{textDecoration: "none"}} href="https://tjournal.ru/u/85142" className="footer__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">qq на TJournal </a>
                <a style={{textDecoration: "none"}} href="https://dtf.ru/u/150653" className="footer__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">qq на DTF </a>
            </span>
        </div>
        <Divider/>
        <a href="https://moskovskiy.org"><img className="footer__logo" src={logo}/></a>
    </div>
    </>
);
}
  
export default Footer;
  
