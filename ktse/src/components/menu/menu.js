import React, { useState, useEffect } from 'react'
import icon from '../../media/icon.png'
import egg from '../../media/egg.png'
import bell from './bell.svg'
import bellred from './bellred.svg'

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"
import axios from 'axios'

import './menu.css'

function endings(value) {
    if (/^-?\d*0$/.test(value)) return 'уведомлений'
    else if (/^-?\d*1\d$/.test(value)) return 'уведомлений'
    else if (/^-?\d*1$/.test(value)) return 'уведомление'
    else if (/^-?\d*[2-4]$/.test(value)) return 'уведомления'
    else if (/^-?\d*[5-9]$/.test(value)) return 'уведомлений'
    else return 'уведомлений'
}

function Menu(props) {

    let token = localStorage.getItem('session-token')
    let loggedIn = false
    let id = -1

    if ((token != null) && (token != "null")) {
        //console.log("authorized with token =", token)
        id = localStorage.getItem('user-info')
        //console.log("id", id)
        loggedIn = true
    } else {
        //console.log("not authorized")
        //console.log("not authorized with token =", token)
    }

    let [userInfo, setUserInfo] = useState({})

    useEffect(() => {

        if (loggedIn)
        axios.get('https://api.stonks.xyz/api/v1/user/info/' + id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setUserInfo(response.data.user)
                //console.log(JSON.stringify(userInfo))
            })
            .catch(function (error) {
                setUserInfo({})
            //loadStonk(getStock(JSON.stringify(error.message)))
        })

    }, [userInfo.balance])

    //let [notificationsCount, setNotificationsCount] = useState(2)

    return (<>
    <div className="menu__bg"/>
    <div className={(window.location.href == "https://stonks.xyz") ? "menu inverted" : "menu"}>
        <NavLink to={loggedIn? "/listing":"/"}><img className="menu__icon" src={
            (window.location != "https://stonks.xyz/investor/2") ? icon : egg
        }/></NavLink>
        {loggedIn ? <>{userInfo.avatar && <a href={"/investor/"+id}>
            <img src={userInfo.avatar} style={{borderColor: ("#" +  userInfo.color)}}className="menu__avatar"/>
            <div className="menu__status">
                <span className="menu__status__worth">{props.balance /*userInfo.balance*/}</span>
                <span className="noMobile"> {userInfo.name}</span>
            </div>

            {<NavLink to="/hub"><div style={(props.alertCount > 0)?{backgroundColor: "#d40303", color: "white"}:{}} className="menu__notifications">
                {((props.alertCount == 0)? "Нет" : props.alertCount) + " " + endings(props.alertCount)}
            </div></NavLink>}

            {<NavLink to="/hub"><div className="menu__notifications--mobile">
            {(props.alertCount > 0) ?<img src={bellred}/> : <img src={bell}/>}
            </div></NavLink>}

        </a>}</> : <>
            <a href="/login" className="menu__status">
                <span className="menu__status__worth"> Войти </span>
            </a>
            </>
        }
        <div className="menu__tabs">
            <NavLink to="/listing" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">{loggedIn? "Акции":"Акции на бирже"}</NavLink>
            {(props.isTrading && loggedIn) && <NavLink to="/buy" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Заявки</NavLink>}
            {<NavLink to="/investor" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">{loggedIn? "Инвесторы":"Инвесторы биржи"}</NavLink>}
            {loggedIn && <NavLink to={"/pro"} className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Stonks Pro</NavLink>}
            {!loggedIn &&<NavLink to="/about" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">FAQ</NavLink>}
            {loggedIn && <NavLink to="/ipo" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">IPO</NavLink>}
            {!loggedIn && <NavLink to="/version" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Что нового</NavLink>}
            {!loggedIn &&<NavLink to="/policy" className="menu__tabs__tab" activeClassName="menu__tabs__tab menu__tabs__tab--selected">Правила использования</NavLink>}
            <div className="menu__tabs__stop"> </div>
        </div>
    </div>
    </>
);
}
  
export default Menu;
  
