import React from 'react'
import './text.css'

function Text(props) {
return (
    <div className={(!props.smol)?"text":"text text--smol"}>
        {props.text}
    </div>
);
}
  
export default Text;
  