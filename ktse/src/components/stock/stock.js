import React from 'react'
import './stock.css'

function Stock(props) {

    let color = "#000000"

    if (props.color < 0) {
        color = "#E52046"
    }

    if (props.color > 0) {
        color = "#13C531"
    }

    return (
        <div style={props.style} className={(props.nohover)?"stock stock--nohover":"stock"}>
            <img className="stock__icon" src={props.image}/>
            <div className="stock__left">
                <div style={{maxWidth: (props.nowrap)? "100vw" : "40vw"}} className="stock__left__name">{props.name}</div>
                <div className="stock__left__ticker">{props.ticker}</div>
            </div>

            <div className="stock__right">
                <div className="stock__right__value">{props.value}</div>
                <div style={{color: color}} className="stock__right__change">{props.change}</div>
            </div>
        </div>
    );
}
  
export default Stock;
  