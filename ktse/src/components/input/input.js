import React from 'react'
import './input.css'

function Input(props) {
    return (
        <div className="input">
            <div className="input__header">{props.header}</div>
            <input type={props.type} pattern={props.pattern} placeholder={props.hint} value={props.value} onChange={props.onChange} className="input__field"/>
        </div>
    )
}
  
export default Input;
  