import React from 'react'
import './row.css'

function Row(props) {
    let color = "#0A0425"

    if (props.color < 0) {
        color = "#E52046"
    }

    if (props.color > 0) {
        color = "#13C531"
    }

    return (
        <div style={props.style} className={props.large ? "row row--large" : ((props.canhover)?"row--canhover row":"row")}>
            <span className={props.large ? "row__key row__key--large" : "row__key"}>{props.name}</span>
            <span style={{color: color}} className="row__value">{props.value}</span>
        </div>
    );
}
  
export default Row;
  