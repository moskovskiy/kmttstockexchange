import React from 'react'
import './view.css'
import Divider from '../divider/divider'

function View(props) {
    let title = (props.title)?(props.title+" – Stonks Exchange"):"Биржа пользователей Stonks Exchange"
    document.title = title
    //document.head.title = document.title
    //document.head.descript
    document.getElementsByTagName('meta')["description"].content = title
    return (
        <div style={props.style} className={(props.invert)?"view invert":"view"}>
            <div className="view__header__wrapper">
                {((props.header!="") && props.showBack) &&
                        <button onClick={(props.backAction)?props.backAction:()=>{window.history.back()}} className="view__back">{"<-"}</button>
                }
                <div className="view__header">
                    <span className={props.insane ?"view__header__text view__header__text--insane":"view__header__text"}>
                        {props.header}
                    </span>
                </div>
            </div>
            {props.children}
        </div>
    );
}
  
export default View;
  