import React from 'react'
import './button.css'

function Button(props) {
return (
    <button className="button" onClick={props.onClick} style={{color: props.color, backgroundColor: props.backgroundColor}}>
        {props.text}
    </button>
);
}
  
export default Button;
  