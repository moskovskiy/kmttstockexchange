import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"

import React, { useEffect, useState } from 'react'

import Menu from '../components/menu/menu'
import Footer from '../components/footer/footer'
import Alert from '../components/alert/alert'

import Landing from './landing'
import Auth from './auth'
import Listing from './listing'
import Profile from './profile'
import Inverstors from './investors'
import Stock from './stock'
import Buy from './buy'
import Investor from './investor'
import Sell from './sell'
import About from './about'
import NoRoute from './404'
import Fraud from './policy'
import Version from './updates'
import Login from './login'
import Pro from './pro'
import Ipo from './ipo'
import Hub from './hub'
import Token from './token'

import axios from 'axios'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}
function Views() {

  let token = localStorage.getItem('session-token')
  let [status, setStatus] = useState({balance: "", is_trading: false, unread_notifications: 0})
 

  //let [userInfo, setUserInfo] = useState({})

  useEffect (() => {
    axios.get('https://api.stonks.xyz/api/v1/market/status', {
    headers: { 'Authorization': localStorage.getItem('auth-token'),
    'Content-Type' : 'text/plain' }
    })
      .then((response) => {
        setStatus(response.data)
        localStorage.setItem('open', response.data.is_trading)
        console.log(status)

        if ((token != null) && (token != 'null')) {
        } else {
          localStorage.setItem('zoom', 'week')
        }
      })
      .catch(function (error) {
        setStatus({})
      })

    const interval = setInterval(() => {
      (status.isTrading != false) &&
      axios.get('https://api.stonks.xyz/api/v1/market/status', {
        headers: { 'Authorization': localStorage.getItem('session-token'),
        'Content-Type' : 'text/plain' }
      })
      .then((response) => {
        setStatus(response.data)
        //localStorage.setItem('open', response.data.is_trading)
        //console.log(status)
      })
      .catch(function (error) {
        setStatus({})
      })
    }, 1000)

    return () => {
        clearInterval(interval);
    }
  }, [])

  //console.log(status)
  
  return (
    <div className="App">
      <Router>
        <Menu isTrading={status.is_trading} balance={fin(status.balance)} alertCount={status.unread_notifications} avatar="https://clck.ru/Sc4ms"/>
        {status.is_trading ? 
          <Switch>
          <Route exact path="/" component={Landing}/>
          <Route exact path="/listing" component={Listing}/>
          <Route exact path="/listing/:id"
            render={(props) => (
              <Stock {...props} open={true} />
            )}
          />
          <Route exact path="/investor" component={Inverstors}/>
          <Route exact path="/investor/:id" component={Investor}/>
          <Route exact path="/buy" component={Buy}/>
          <Route exact path="/buy/:id/" component={Sell}/>
          <Route exact path="/me" component={Profile}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/policy" component={Fraud}/>
          <Route exact path="/pro" component={Pro}/>
          <Route exact path="/ipo" component={Ipo}/>
          <Route exact path="/version" component={Version}/>
          <Route exact path="/hub" component={Hub}/>
          <Route exact path="/negovoritnikomu" component={Token}/>
          <Route exact path="/auth/:token" component={Auth}/>

          <Route component={NoRoute}/>

        </Switch> : <Switch>

          <Route exact path="/" component={Landing}/>
          <Route exact path="/listing" component={Listing}/>

          {/*<Route exact path="/listing" component={() => 
            <div style={{marginTop: 100}}>
                <div className="">
                    <div className="view__header--alone">
                      <div style={{fontSize: 20, fontWeight: 900}}>Торги сейчас недоступны</div>
                      <div style={{marginLeft: 1.5, marginTop: 10, fontSize: 12, fontWeight: 600}}>Биржа работает с 8:00 до 23:59 GMT+3</div>
                    </div>
                    <div style={{marginTop:-90}}>
                      <Listing isNotTrading={true}/>
                    </div>
                </div>
            </div>
          }/>*/}

          <Route exact path="/listing/:id"
            render={(props) => (
              <Stock {...props} notOpen={true} />
            )}
          />
          <Route exact path="/buy/:id/" 
            render={(props) => (
              <Stock {...props} notOpen={true} />
          )}/>
          <Route exact path="/investor" component={Inverstors}/>
          <Route exact path="/investor/:id" component={Investor}/>
          <Route exact path="/me" component={Profile}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/policy" component={Fraud}/>
          <Route exact path="/pro" component={Pro}/>
          <Route exact path="/ipo" component={Ipo}/>
          <Route exact path="/version" component={Version}/>
          <Route exact path="/hub" component={Hub}/>
          <Route exact path="/negovoritnikomu" component={Token}/>
          <Route exact path="/auth/:token" component={Auth}/>

          <Route component={NoRoute}/>
          </Switch>
        }
        <Footer/>
      </Router>
    </div>
  );
}
  
export default Views;
  