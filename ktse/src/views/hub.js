import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React, { useState, useEffect } from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Stock from '../components/stock/stock'
import bg from '../media/bg.png' 
import axios from 'axios'

function Hub(props) {

    let [notifications, setNotifications] = useState({})

    useEffect(()=> {
        axios.get('https://api.stonks.xyz/api/v1/notifications/', {
            headers: { 'Authorization': localStorage.getItem('session-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setNotifications(response.data)
            })
            .catch(function (error) {
        })
        axios.post('https://api.stonks.xyz/api/v1/notifications/read/', {}, {
            headers: { 'Authorization': localStorage.getItem('session-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
            })
            .catch(function (error) {
                alert(JSON.stringify(error.message))
            })
    }, [])
  
    return (
        <View title="Уведомления" header="Хаб">
            
            {/*<div className="view__left">

            <Text text="Текущие уведомления"/>
            <Button text="Прочитать всё" color="#353DF6" backgroundColor="#D0EBFF" onClick={()=>{
                    axios.post('https://api.stonks.xyz/api/v1/notifications/read/', {}, {
                        headers: { 'Authorization': localStorage.getItem('session-token'),
                        'Content-Type' : 'text/plain' }
                        }).then((response) => {
                            window.location.reload()
                        })
                        .catch(function (error) {
                            alert(JSON.stringify(error.message))
                            window.location.reload()
                        })
                }}/>

            </div>*/}

            <div className="view__left">
                <Text text="Уведомления"/>
                {notifications.notifications && 
                notifications.notifications.map(el=><Stock
                    image={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABRFBMVEX///9ES1T/YUT8yj1BSFE/RlBBS1Q/R1A5QUv8yTc9RU/8/PxDSlQ7Qkw9SlTy8vNhZm1dY2o1SlVMU1zj5OVyd33d3t+JjZLv8PHsX0ZWXGR8gIbX2No0PEfQ0tRvdHr/9+PcXUeztbn8yC+AhIlLTFPhXUefoqeRlZmqrLDCxMf8zUdSWGD913LCoUVQU1KDUk9iTlL1YEVxUFD//fb94qD93pL+7cL+6LD+8tH/++6ZnKDzx0796KzPW0hVTVOSU051UFG1WEvEWUr/jnycVU3bw4eLgWXDplt2cmPivFeejFlATV/MrFhnaWXJrGDpwE792XqUhVz81F9vaVWck3XcvWORjXswPlK9n1KumF57clX92HTavWz93IjUwpLPqTxfXU//1s7NnZb/qpqWgYH/VDCoV0z/w7r/7Oj/pJb/emNU7qMLAAAMVklEQVR4nO2d63/aOBaG40bGxsYYYm7B2E5cCNhhtkDSmTZkgSTNtnPrzLTb7mWmszuz3cl09///vuZmyWBuRkJOVu+H9tcmIXo4R0fSKyHv7TExMTExMTExMTExMTExMTEx/Z8oZ9ftHO1GEJRa1zgupdVpt4OY5IYBOI4DSlem3RRCMkVuJFA2aTeFkIrKmJATHdpNIaQ8PyFUGirttpCRLk0IBf2B1tPuNEtBpkC7LWRkTyqNN2BUaLeFjCoGNy01D7SYlnif0H6YI2IpC6bF9IGO+WpemBBK1Yc5XKiNaTHlMyXajSEi2ZkWU6A90AHR9Iup8UAJK5xP6Hohpd0cAsr5xRRkdF2vNuqOmyuVHk7Vkc3MlJADguRJEQ1DsxpuJfcQKHOuk9e4eXmsBmfV3dz9zlrZdHRN5EMAJ6sNRcvXzXvM6BazQFiIN4Hky1X3njKausaDFXyjfOU1y6Xd2AjK6cJafONAcvp9WzqqDietBkMkgfp9mtHJZl5ZO35Tifn7U3Jku6yEUwBB4AUBhNNL2n1ZPpYa3HwBBYI3zHNZK5/XrUyWM0RpHhOI3XuRqbmqONd0ydDyXdesFHIjFSqmUyyL4hwkqN6DglPIzwaQN7SGNw8NZqCslir1sjFbjkA+9oiVjJAKZqdoueqC/iVXdEMKfLu3SI45YiUTqDEAaCtc4FKjmQowSuVYIxbyAUBBqy73D8+Pnn7+p5dPAoxSJsbL5JKOFpkUsOzlq6OLp8nHycenr778Bn1fRD2+a6oGCghAd0Uwzj9P7g+V/Orrb9CyasTWdLSlAKC7KhRfXI4J95Onr5qBJZa9k/ZuLFNDAsFrqx38pxPAob59jbw9IBtL+1+2kDBI5TU2YT5HCPffo4i8HsfJTReJoLDWqIbGcD/5/kv0HYrhdrFZhoQgs1aWfXG6H0B8DRH5TOw24tQiTDJQXm/Jfv5zMoD4XROOi0o3bkOGi5aZdUvh0c+PA4iv4LgI1qhUO1WpCkOoNNYezi6enQbC+PKv8FWq8RoUXaQTapvk1/O3SBiT32dgVxRi1RNVJITGZr7Z0Q9opn4L81SqEmpsJBXgdE0pbvizF2i9uXyHvFVxmoH7x56ibBGiiMmvsn6eKg0STY0mFfZCpb55gTj6DCKefu3nKWjGp9a4/hYoWGe2NqfncOhPfgvHfRAfJ9w/1xX16NozWG1O3/jDPti0SxOTDKciWrS3/fytn6fJV/7MBsTmYIPru0lCPmL9ew6DePnnaZqmmnFJ067vH0pRTzqf/wCD+M63bfi4VFNrSgiiv+nPIeGHJz6hFY8hUfUPIgjRW3SBjBh+Nd3iHcMqs+kTRp8tn//xMUxTvzQL8Tjf78KDCN3orwLHxOR7f4UhxeNUsQPHii08MnRiU/bHi2YsVokN/zzQVt0GujbJv8AZhBOHmZu/cgJbbTl8EZ6mcRj0IWF2G28lkKb+VD4OR8Nl3R8OM1ulFLRPT2E1NWKw8636nxgBma1e6BlcYcClvhCDbZqST8hvR3gEp9/fw7N+KfrjBTbCPTg3vUTTFE8ztxA+wmf7SJr6c9M8nmZuIXyEF3C8+AqmqUi9I+Ij3IPjxeUbP01F6mmKkRCaGcm/wWpqYWnmFsJIeA4JkY0oiXaaqvgI996GpalEO01xEiKLxL/7azJBx9HMLYST8AgSfvfaX0KVKc++cRIiruLpj35H3GbZiUM4CdFq+obzB33KzjBWwiN/VoNYblsYXFiEbW0x1MVPME2/nBLSdoaxxhBN03dwRKR7DAxrDFHL7QM0FaNuFuARXsJQyy0VccMHk/Bm6d5TmKZxsdzwxjDccqObppgJwy03jqYzDD+MjoUQPbEIvQyRZppijmHQcoNeBsW5Ke4YIpbbJWK5UTzBjzuGez+HWW407yfCHcOg5eZXU4teR8ROeOSfV0QtN4NeR1R1zIR7P4VabvS2g1VcOzO+kPND0HIDWSyvHUX4CS/gxO396xikKbbdNSh/vEjGwhkmQAgtt31oudHbwCBAiKQpYrk1ac2+CRCizvCP8NAwrTQlQYh4GV/DNKXlDJMgPAo75Ubtyj4ShMgpN2i5UXOGSRAGLDefUFr/oypYRYQQtdw46AzTSVMihIiXcZqFaUrHciNCuMByi/BRBwwiQ4hYbh+g5UZnA4MMIZKmSWi5pahYbmQIEcsNSVORSpqihBjPFCAfoUWcYYvGEooQYbjlBmgcxiREiFhup5SdYVKEqDMM0xRnV19XpAiPLv1Sg1puNMYL/5NreAlRy+0dXcuNFOEzuEUDnWFQxvkr1pSO5yT7nC58wIDlRiFNSREusNwobGAQI0QtN7iBgclY30TECBHL7cNreGph92lKjBC13F5Cy233aUqOMNRyE3bvDJMjRC036Axndz43JUd4/llyKtRy23maYvpkV1CyWsoVKv/45x+mgoTCzg9jIoTbrt5KuYrp2k692yhW9bxlvf7llycj1Z7U/EoDyg3Hdgs7PN+OgVDNmbbTLepWJpNtahyQFEkSeIEHKV8cFJA4rZzJFx1zR5TbEKoVt17MZ8tNzauVguAxLbigNqgUBwAveJzFnfiL0QjlSr2aAYY4jJaHlVpKtAgUSKLmkA9kJMKCZYjS4sva1xcQyyuvL9xWUQhzG16GvUw83yA8kYtCqOMD5Ib3gZK14FYRyq5Td4JWbkHb+Lrv5Yhkb81aQagWyykllQ0cRnfDngexFSJRM3wFoT66uRsI8CaWT7/+q7asuRyX8JT2dDCndNr70vwPSER3wJcTupNrwMbXBH769eOLzqNHj9pzrUxMNfpXv33WGgxu765vbnq93vFYvZub69vWWbs2+Xbkp8nemrWc0PcaE//++OLRb789Guk6nYBkXlxqtX6/3b5qeUy948POo+XqnNzcttr9ACHJcrqc0L8nMD34DWnkoJYeJWJtFK0x2AquWczeoI8QkhwTkXsxTNN1g6u3T9a0aiYGgQZeD66uWgMvDXsnq0K2QId3MNfJnuuDdyZqliaCjL8DJv/6+8cXt+nJF2vXMzE4OdkwakHdtJCeKBFdUSG3QvKp4dNxRvfwfPr9Py/+60WnUxu3I30WMVTh6rX6aQ7KILrur87MT4Dmfvo4ohu3JXHgVZOD2glmPrSUSmS9m1lCLtEKNOdw0E/0bzFG8OaMC46JPEdw1ibnnObsDAxzQgbUObzlDmYGU4Hgjbxyoa7NP4cjcUWI0BsHW4lZPk5Y82LmKCrUm/N8c+MCLnnDfDudnvttfIbYA7FKdiaMj0v0j0ng3Z1x6fkZKZCqpMqobOohD8wZ9sLEDW684+vBWT8EzyuivEOqyMhOOYwvcZBuYY7g8d1Vuxa6oOCAYVVIZaish3hiifRB+26recqsOjcDbyURSjfkI+jQyNm5Jx4lErX+AOOo3ukc3/a99eACvBTPZQleA1aylDm8dgtb9+scnvTuzryED4cbhk/SLGId0JNcnOmC6VrrDlP4Do9717dntYO5UQ+RIJWLK55gs6XsoI+U5lo3OHpf56R3fddq1xZm5liSYNUJO6QlHXVyE+mr7fk6x0N/4qq/LDPH4RO5olsgfS7KRT5izaXbW/Id9kbeS7+WWB46bmhvK5Zd2MHpRAeuJRLcbXS+EVu736/N2kqhdIJoZJ3coueb4VUdKaS3Ecg6Hpo3R5lag6vYhg8qBZyWIVk7ZwmRsdCrotfHh4edzrLVhPfVzuHh4cnx2AxMjNlWgk1iJ6XKVtHe6clZGx0rvHnMQaI9tpTGvubJSBOHs9e7ub6+ux0MXc6pobse2lC8xDetYn1XG6G+ApVmgjlyp7mx6zlRf9jD0hOjOtypXk4nalbRIV84Q6RWFz3FODGrzaAgnSJymYZt7qJuhsqc8y3wCUiikcp33UKO6hVmzubPwl0TztB0p1LazZiwVLay6nHbG6EBYbiRX9Z3X1MWq2Ct98ztVWheteS0ctba3cmRtSXbelkSIp2gGKLxgqTww0Mx1UbdNuNwZ3eIVLee10Rlo1COyETJQ9OLXcd2Kzn6PW6pcq7dzTdFQ5GWcXrdTPK4vG8bkdVt1zUrs88Hjq/UXMG0h8eaNMXwJA6lKKO/xOF/GApXzlijiJmV4TOd7w0ZKllWS6VSLldxXdt2HKde9/6wbXsYKg/Jk6qq8n0kY2JiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmLaWP8Dr6lYA5fBySMAAAAASUVORK5CYII="}
                    name={el.title}
                    ticker={el.text}
                    nowrap={true}
                    style={{backgroundColor: el.unread?"#edecf3":"white"}}
                />)}
            </div>


            <div className="view__left">

            </div>
        </View>
    );
}

export default Hub;

  
