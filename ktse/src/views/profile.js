import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React, { useState, useEffect } from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'

import axios from 'axios'

import Loader from '../components/loader/loader'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}

function Profile() {

    let token = localStorage.getItem('session-token')
    let loggedIn = false
    let id = -1
    let [is404, display404] = useState(false)
    let [headerText, setHeaderText] = useState("")


    if ((token != null) && (token != "null")) {
        //console.log("authorized with token =", token)
        id = localStorage.getItem('user-info')
        //console.log("id", id)
        loggedIn = true
    } else {
        //console.log("not authorized")
        //console.log("not authorized with token =", token)
    }

    let [userInfo, setUserInfo] = useState({})

    useEffect(()=>{
        axios.get('https://api.stonks.xyz/api/v1/user/info/' + id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setUserInfo(response.data)
                //console.log(JSON.stringify(userInfo))
            })
            .catch(function (error) {
                setUserInfo({})
                setHeaderText("Ошибка")
                display404(true)
            //loadStonk(getStock(JSON.stringify(error.message)))
        })
    }, [])

    return (
        <View header={(userInfo.user)? userInfo.user.name : headerText}>
            {is404 && <>
                <Text text="Не удаётся загрузить данные"/>
                <Button text="Назад" onClick={()=>window.history.back()}/>
            </>}

            {(userInfo.user) ? <>
            <br/>
            <Text text={"Всего: ¢" + fin(userInfo.net_worth)}/>
                
            <Row name="Баланс: " value={"¢" + fin(userInfo.user.balance)} color={0}/>
            <Row name="Стоимость портфеля: " value={"¢" + fin(userInfo.portfolio_price)} color={0}/>
            <br/>
            <Button onClick={()=>document.location.href=("/investor/"+id)} text="Мой профиль инвестора" color="#fff" backgroundColor={"#"+userInfo.user.color}/>
            <Button onClick={()=>{localStorage.setItem('session-token', null); document.location.href="/"}} text="Выйти из аккаунта" color="#353DF6" backgroundColor="#D0EBFF"/>
            
            <Divider/>
            <Text text="Выпуск акций пока недоступен"/>
            <Divider/>
            
            <Row name="Привязанный аккаунт" value={userInfo.user.platform + "/" + userInfo.user.platform_id} color={0}/>
            {/*<div className="nano">{token}</div>
            
            
            <Text text="Статистика"/>
            <Row name="На бирже" value="1200 пользователей" color={1}/>
            <Row name="Акций пользователей" value="50000000 шт" color={-1}/>*/} 
            </> : <Loader/>}
        </View>
    );
}

export default Profile;

  