import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"

import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'

import React, { useState, useEffect } from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Tabs from '../components/tabs/tabs'
import StockOne from '../components/stock/stock'
import Loader from '../components/loader/loader'
import Minik from '../components/minik/minik'
import Alert from '../components/alert/alert'

import { Line } from 'react-chartjs-2'
import Chart, { CrosshairMode } from 'kaktana-react-lightweight-charts'


const LineChart = (props) => {
    const options = {
        color: '#eee',
        
        timeScale: {
            borderVisible: false,
            //borderColor: '#eee',
            visible: true,
            timeVisible: true,
            secondsVisible: true,
            //rightOffset: 12,
            //barSpacing: 20,
            //fixLeftEdge: true,
            //rightBarStaysOnScroll: false
        },
        /*
        crosshair: {
            horzLine: {
                visible: true,
                labelVisibinle: true,
                mode: 0,
                style: 3
            },
            vertLine: {
                visible: true,
                style: 0,
                width: 2,
                mode: 0,
                color: 'rgba(32, 38, 46, 0.1)',
                labelVisible: true
            }
        },*/
        
        layout: {
            backgroundColor: "white",
            textColor: "#110011",
            fontSize: 12,
            fontFamily: "Manrope",
        },

        grid: {
            horzLines: {
                color: '#eee',
            },
            vertLines: {
                color: '#ffffff',
            },
        },

        priceScale: {
            position: 'right',
            mode: 0,
            borderVisible: false,
            invertScale: false,
            alignLabels: true,
            autoScale: true,
            borderVisible: false,
        },

        localization: {
            locale: 'ru-RU',
        },
    }

    let test = [{"time":0,"value":0}]
    
    let tt = props.volumes.prices ? Object.assign(props.data) : test

    const areaSeries = [{
        title: "Стоимость акции",
        options: {
            overlay: true,
            lineColor: props.color,
            topColor: (props.color+"aa"),	
            bottomColor: "#ffffff00",
            lineType: 2,
            lineWidth: 4
        },
        data: tt
    }]
    
    let test2 = [{"time":1609696737,"value":250.5,"buy":251.0,"sell":250.0,"volume":0},{"time":1609697034,"value":175.0,"buy":100.0,"sell":250.0,"volume":0},{"time":1609698265,"value":200.0,"buy":150.0,"sell":250.0,"volume":0},{"time":1609705081,"value":225.0,"buy":150.0,"sell":300.0,"volume":0},{"time":1609723856,"value":262.5,"buy":225.0,"sell":300.0,"volume":0},{"time":1609725976,"value":487.5,"buy":225.0,"sell":750.0,"volume":0},{"time":1609726019,"value":862.5,"buy":225.0,"sell":1500.0,"volume":0},{"time":1609728387,"value":612.5,"buy":225.0,"sell":1000.0,"volume":0},{"time":1609728475,"value":162.5,"buy":225.0,"sell":100.0,"volume":0},{"time":1609728492,"value":175.0,"buy":250.0,"sell":100.0,"volume":0},{"time":1609728565,"value":875.0,"buy":250.0,"sell":1500.0,"volume":0},{"time":1609728710,"value":525.0,"buy":250.0,"sell":800.0,"volume":0},{"time":1609728986,"value":375.0,"buy":250.0,"sell":500.0,"volume":0},{"time":1609729178,"value":175.0,"buy":250.0,"sell":100.0,"volume":0},{"time":1609729210,"value":375.0,"buy":250.0,"sell":500.0,"volume":0},{"time":1609729499,"value":5225.0,"buy":9950.0,"sell":500.0,"volume":0},{"time":1609729513,"value":375.0,"buy":250.0,"sell":500.0,"volume":0},{"time":1609729525,"value":875.0,"buy":250.0,"sell":1500.0,"volume":0},{"time":1609729641,"value":725.0,"buy":250.0,"sell":1200.0,"volume":0},{"time":1609735443,"value":712.5,"buy":225.0,"sell":1200.0,"volume":0},{"time":1609741361,"value":362.5,"buy":225.0,"sell":500.0,"volume":0},{"time":1609744292,"value":712.5,"buy":225.0,"sell":1200.0,"volume":0},{"time":1609744310,"value":312.5,"buy":225.0,"sell":400.0,"volume":0},{"time":1609747443,"value":315.0,"buy":230.0,"sell":400.0,"volume":0},{"time":1609757400,"value":312.5,"buy":230.0,"sell":395.0,"volume":54},{"time":1609762326,"value":315.0,"buy":230.0,"sell":400.0,"volume":33},{"time":1609762343,"value":715.0,"buy":230.0,"sell":1200.0,"volume":25},{"time":1609763265,"value":725.0,"buy":250.0,"sell":1200.0,"volume":40},{"time":1609763401,"value":425.0,"buy":250.0,"sell":600.0,"volume":50},{"time":1609763724,"value":725.0,"buy":250.0,"sell":1200.0,"volume":40},{"time":1609763741,"value":625.0,"buy":250.0,"sell":1000.0,"volume":51},{"time":1609765937,"value":625.5,"buy":251.0,"sell":1000.0,"volume":58},{"time":1609770843,"value":618.0,"buy":251.0,"sell":985.0,"volume":50},{"time":1609773535,"value":525.5,"buy":251.0,"sell":800.0,"volume":60}]

    //console.log(props.volumes.prices == test)
    //console.log("vol", props.volumes.prices)
    let kk = props.volumes.prices ? Object.assign(props.volumes.prices) : test
    //props.volumes.prices && console.log("vol", )
    //console.log("test", test)
    
    const histogramSeries = [{
        options: {
            overlay: true,
            fillColor: (props.color+"aa"),
            topColor: (props.color+"aa"),	
            lineType: 2,
            lineWidth: 4,
            base: 0
        },
        data: kk//test.map(function (el) {let nel = el; nel.color = "#eeeeff"; nel.value = nel.volume; return nel}) histogramSeries={histogramSeries}
    }]
    

    function updatePrice () {
        //console.log("HI!")  options={options} 
    }

    try {
    return (
        <div className="chart">
        <Chart options={options} areaSeries={areaSeries} histogramSeries={histogramSeries} autoWidth height={250} />
        </div>
    )}
    catch (err) {
        return (
            <Text text={JSON.stringify(err)}/>
    )}
}

const optionsOld = {
    responsive: true,
    maintainAspectRatio: true,
    legend: {
        display: false
    },
    scales: {
        xAxes: [{
            gridLines: {
                drawOnChartArea: false
            }
        }],
        yAxes: [
        {
            ticks: {
            beginAtZero: false,
            },
            gridLines: {
                drawOnChartArea: false
            },
            markings: {
                drawOnChartArea: false
            }
        },
        ]
    }
}

const LineChartOld = (props) => (
  <div className="">
    <Line data={props.data} options={optionsOld} />
  </div>
)

let color = '#494fe6'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}

function getStock(err) {
    //alert(JSON.stringify(err))
    return {
        ticker: "ошибка",
        name: "не может быть загружен",
        platform: "ошибка " + String(err),
        price: 0,
        dayprice: 0,
        monthprice: 0,
        stocks_count: 0,
        buy: 0,
        ipo_date: 0,
        posts: 0,
        comments: 0,
        karma: 0,
        subscribers: 0
    }
}

function checkColor (color) {
    let rgb = parseInt(color, 16)
    let r = (rgb >> 16) & 0xff
    let g = (rgb >>  8) & 0xff
    let b = (rgb >>  0) & 0xff

    let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b

    if (luma < 200) return true
    return false
}

function Stock(props) {

    let stock = getStock(props)
    let token = localStorage.getItem('session-token')
    let id = localStorage.getItem('user-info')

    let loggedIn = ((token != null) && (token != 'null'))
  
    let [index, setIndex] = useState(0)
    let [is404, display404] = useState(false)
    let [headerText, setHeaderText] = useState("")
    let [stonk, loadStonk] = useState({})
    let [prices, loadPrices] = useState({})
    let [portfolio, loadPortfolio] = useState({})
    let [mentions, setMentions] = useState({})
    let [glass, setGlass] = useState({})
    let [graphData, setGraphData] = useState({})
    let [volumeData, setVolumeData] = useState({})
    let [zoom, setZoomFx] = useState("week")
    let [price, setPrice] = useState("")
    let [alertText, setAlertText] = useState("")
    let [allData, setAllData] = useState({})

    function setZoom (scale) {
        //setZoomFx(scale)
        localStorage.setItem('zoom', scale);
        console.log("Zoom adjusted to ", scale)
    }
    
    useEffect(() => {
        axios.get('https://api.stonks.xyz/api/v1/stock/info/' + props.match.params.id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
            })
        .then((response) => {
            setAllData(response.data)
            loadStonk(response.data.stock)
            color = stonk.color
        })
        .catch(function (error) {
            //loadStonk(getStock(JSON.stringify(error.message)))
            display404(true)
            setHeaderText("Ошибка загрузки")
            setAlertText(error.message)
        })

        axios.defaults.adapter = require('axios/lib/adapters/http')

        axios.get('https://api.stonks.xyz/api/v1/market/stock/price/' + props.match.params.id, {responseType: 'stream', adapter: httpAdapter})
        .then((response) => {
            const stream = response
            if (stream.data) {
                loadPrices(stream.data)
            }
        })

        axios.get('https://api.stonks.xyz/api/v1/stock/disccusses/' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setMentions(response.data)
            })
            .catch(function (error) {
                setAlertText(error.message)
        })

        axios.get('https://api.stonks.xyz/api/v1/market/stock/graph/' + props.match.params.id + '?interval=' + zoom
        ,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setGraphData(response.data)
                response.data.prices.map(function (el) {let nel = el; nel.color = "#ddddee"; nel.value = nel.volume; return nel})
                setVolumeData(response.data)
            })
            .catch(function (error) {
                setAlertText(error.message)
        })
    
        axios.get('https://api.stonks.xyz/api/v1/market/orders/book/' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setGlass(response.data)
            })
            .catch(function (error) {
                setAlertText(error.message)
        })

        if (localStorage.getItem('user-info') != null)
        axios.get('https://api.stonks.xyz/api/v1/user/check_stock/?user_id=' + id + '&stock_id=' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                loadPortfolio(response.data)
            })
            .catch(function (error) {
                setAlertText(error.message)
        })

        function update () {    
            axios.get('https://api.stonks.xyz/api/v1/stock/info/' + props.match.params.id, {
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                    'Content-Type' : 'text/plain' }
                })
            .then((response) => {
                setAllData(response.data)
                loadStonk(response.data.stock)
                color = stonk.color
            })
            .catch(function (error) {
                //loadStonk(getStock(JSON.stringify(error.message)))
                display404(true)
                setHeaderText("Ошибка загрузки")
                setAlertText(error.message)
            })

            axios.defaults.adapter = require('axios/lib/adapters/http')

            axios.get('https://api.stonks.xyz/api/v1/market/stock/price/' + props.match.params.id, {responseType: 'stream', adapter: httpAdapter})
            .then((response) => {
                const stream = response
                if (stream.data) {
                    loadPrices(stream.data)
                }
            })

            axios.get('https://api.stonks.xyz/api/v1/stock/disccusses/' + props.match.params.id,{
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    setMentions(response.data)
                })
                .catch(function (error) {
                    setHeaderText("Ошибка загрузки")
                    setAlertText(error.message)
            })
    
            axios.get('https://api.stonks.xyz/api/v1/market/stock/graph/' + props.match.params.id + '?interval=' + localStorage.getItem('zoom')
            ,{
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    //console.log("Loading graph for zoom: ", localStorage.getItem('zoom'))
                    setGraphData(response.data)
                    response.data.prices.map(function (el) {let nel = el; nel.color = "#ddddee"; nel.value = nel.volume; return nel})
                    setVolumeData(response.data)
                })
                .catch(function (error) {
                    setHeaderText("Ошибка загрузки")
                    setAlertText(error.message)
            })
        
            axios.get('https://api.stonks.xyz/api/v1/market/orders/book/' + props.match.params.id,{
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    setGlass(response.data)
                })
                .catch(function (error) {
                    setHeaderText("Ошибка загрузки")
                    setAlertText(error.message)
            })

            if (localStorage.getItem('user-info') != null)
            axios.get('https://api.stonks.xyz/api/v1/user/check_stock/?user_id=' + id + '&stock_id=' + props.match.params.id,{
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    loadPortfolio(response.data)
                })
                .catch(function (error) {
                    setHeaderText("Ошибка загрузки")
                    setAlertText(error.message)
            })

        }
        
        const interval = setInterval(update, 3000);

        /*const unlisten = window.history.listen(() => {
            window.scrollTo(0, 0);
        });*/
        return () => {
            clearInterval(interval);
            //unlisten();
        }

    }, [])

    let unix_timestamp = stonk.ipo_date
    var date = new Date(unix_timestamp * 1000)
    var day = date.getDate()
    var month =  1 + date.getMonth()
    var year = date.getFullYear()
    var ipoFormatted = day + '.' + month + '.' + year
    
    return ((headerText=="Ошибка")||(alertText!=""))? 
        <View>
            <Alert
                visible={true}
                header="Не удаётся отобразить акцию"
                text={alertText}
                onClose={()=>window.history.back()}
            />
        </View>:
        <View backAction={()=>window.location.href="/listing"} title={(stonk.ticker)? ((stonk.ticker).toUpperCase() + ((price!="")?(" "+fin(price)):"")) : headerText} insane={true} showBack={true} header={(stonk.ticker)? ((stonk.ticker).toUpperCase() + ((price!="")?(" "+fin(price)):"")) : headerText}>

            {(stonk.ticker)?<>

                <div className="view__left view__left--large">
                <Row large={true} name={fin(prices.sell)} value={allData.diff_price+"%"} color={allData.diff_price}/>
                <Divider/>
                <Minik active={(localStorage.getItem('zoom') == "day")} onClick={()=>setZoom("day")} text="1D"/>
                <Minik active={(localStorage.getItem('zoom') == "week")} onClick={()=>setZoom("week")} text="1W"/>
                <Minik active={(localStorage.getItem('zoom') == "month")} onClick={()=>setZoom("month")} text="1M"/>
                {/*<Minik active={(localStorage.getItem('zoom') == "year")} onClick={()=>setZoom("year")} text="1Y"/>*/}
                
                {graphData.prices ?<>
                <LineChart
                    setPrice={setPrice}
                    data={graphData.prices}
                    volumes={volumeData}
                    color={"#"+stonk.color}
                />
                <Row name="Линия – цена, столбики – количество акций" nowrap={true} color={0}/>
                </>: <Loader/>}

            <Divider/>

            </div>
            <div className="view__left view__left--small">

            {/*(loggedIn && props.open) && <>
                {/*color="white" backgroundColor={"#"+stonk.color}*/}
                {/*(stonk.is_trading)
                    ? */}
                    <Button text={"Купить или продать"} backgroundColor={"#"+stonk.color} color={(checkColor(stonk.color))?"white":"black"} onClick={()=>document.location.href="/buy/" + props.match.params.id + ""}/>
                    {/*}: <Text text="Эта акция не находится в обращении"/>
                }
            </>*/}

            {(!loggedIn) &&
                <Text text="Чтобы купить акцию, войдите в аккаунт"/>
            }

            {(loggedIn && (!props.open)) &&
                <Text text="Биржа работает с 09:00 до 23:00 GMT +3"/>
            }
            <br/>
            <br/>

            <Tabs tabs={["Инфо", "Стакан", "Меншоны"]} index={index} setIndex={setIndex}/>

            {(index==2) && <>
               {(mentions.discusses)? mentions.discusses.map(el => <>
                    <a style={{textDecoration: "none"}} href={el.url}>
                        {(el.author != null)? <>
                            <StockOne image={el.author.avatar} name={el.author.name} ticker={el.url}/>
                            <Row nowrap={true} name={el.text}/>
                        </>:<>
                            <Divider/>
                            
                            <Text text={"Упоминание в статье на "+el.platform.toLowerCase()+".ru ->"}/>
                            <Text smol={true} text={el.text.substring(0, 250)+"..."}/>
                            {/*JSON.stringify(el)*/}
                            <Divider/>
                        </>}
                    </a>
               </>) : <>
               <Text text={"Добавляйте упоминания через $"+(stonk.ticker)? ((stonk.ticker).toUpperCase()) : "ОШИБКА"}/>
               </>}
            </>}

            {(index==1) && <>
                <Row value="Цена" name="Продажа"/>
                {Object.keys(glass.sells).map(el=><>
                    <Row value={fin(el)} name={"" + glass.sells[el] + " шт"} color={-1}/>
                    <div style={{width: "100%", borderTop: "1px solid #D0EBFF"}}/>
                </>)}

                <Row value="Цена" name="Покупка"/>
                {Object.keys(glass.buys).map(el=><>
                    <Row value={fin(el)} name={"" + glass.buys[el] + " шт"} color={1}/>
                    <div style={{width: "100%", borderTop: "1px solid #D0EBFF"}}/>
                </>)}
            </>
            }

            {(index==0) && <>
                
                <Row name="Цена продажи" value={fin(prices.sell)} color={0}/>  
                <Row name="Цена покупки" value={fin(prices.buy)} color={0}/>
                
                {loggedIn && <>
                    {portfolio.portfolio && <Row name="В портфеле" value={portfolio.portfolio.amount + " шт"}/>}
                </>}
                
                <a style={{textDecoration: "none"}} href={"https://"+stonk.platform+".ru/u/"+stonk.platform_id}>
                    <StockOne nowrap={true} image={stonk.pic} name={stonk.name} ticker={(stonk.ticker)? ("Автор на " + stonk.platform):""}/>
                </a>
                <Row name="Подписчиков" value={stonk.subscribers} color={0}/>
                <Row name="Карма" value={stonk.karma} color={0}/>
                <Row name="Постов" value={stonk.posts} color={0}/>
                <Row name="Комментариев" value={stonk.comments} color={0}/>

                <Divider />
                <Row name="Дата IPO" value={ipoFormatted}/>
                <Row name="Эмитировано акций" value={stonk.stocks_count + " шт"} color={0}/>


            </>}
            
            </div>
            </>:<Loader/>}
            
        </View>
}

export default Stock;

  
