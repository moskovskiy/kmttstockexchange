import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/bg.png'

function About() {
  
    return (
        <View title="Список обновлений" header="Список обновлений">
            
            <div className="view__left">
            <Divider/>
            <Row name="03.02.2021" value="2021.1.2"/>
            <Row name="• Биржа запущена в публичный доступ" nowrap={true}/>
            <Row name="• Отображаются упоминания в статьях" nowrap={true}/>
            <Row name="• Небольшой редизайн страницы акции" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="30.01.2021" value="2021.1.1b"/>
            <Row name="• Новый вид всплывающих окон" nowrap={true}/>
            <Row name="• Починен баг с графиками" nowrap={true}/>
            <Row name="• Сохраняется выбор диапазона" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="20.01.2021" value="2021.1.0b"/>
            <Row name="• Добавлены уведомления в систему" nowrap={true}/>
            <Row name="• Биржа закрывается на ночь" nowrap={true}/>
            <Row name="• Новый вид графиков" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="04.01.2021" value="2021.0.5a"/>
            <Row name="• Редизайн небольшой" nowrap={true}/>
            <Row name="• Стакан отображается постоянно" nowrap={true}/>
            <Row name="• У транзакций есть дата" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="03.01.2021" value="2021.0.4a"/>
            <Row name="• Профиль находится теперь вместе с инвест. данными" nowrap={true}/>
            <Row name="• Починены баги при покупке" nowrap={true}/>
            <Row name="• Добавлен стакан на экран покупки, в нём можно сразу подставить" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="02.01.2021" value="2021.0.3a"/>
            <Row name="• Добавлен график акций" nowrap={true}/>
            <Row name="• Уменьшено количество запросов к серверу в секунду" nowrap={true}/>
            <Row name="• Добавлен вход по ссылке" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="31.12.2020" value="2020.0.2a"/>
            <Row name="• Добавлен рыночный список акций" nowrap={true}/>
            <Row name="• Добавлена политика антифрода и история обновлений" nowrap={true}/>
            <Row name="• Починен баг с 404 страницами" nowrap={true}/>
            <Row name="• Добавлено отображение акций" nowrap={true}/>
            <Row name="• Добавлена покупка акций в один клик" nowrap={true}/>
            <Row name="• Добавлена возможность смотреть портфели инвесторов" nowrap={true}/>
            <Row name="• Добавлена возможность смотреть текущие заявки на рынке" nowrap={true}/>
            </div>
            <div className="view__left">
            <Divider/>
            <Row name="25.12.2020" value="2020.0.1a"/>
            <Row name="• Базовый функционал" nowrap={true}/>
            <Row name="• Добавлена возможность смотреть статистику по акции" nowrap={true}/>
            <Row name="• Добавлена возможность смотреть собственное состояние на запуске" nowrap={true}/>
            </div>
        </View>
    );
}

export default About;

  
