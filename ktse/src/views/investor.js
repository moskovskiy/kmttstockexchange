import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"

import React, { useState, useEffect } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Tabs from '../components/tabs/tabs'
import StockOne from '../components/stock/stock'
import Loader from '../components/loader/loader'
import Alert from '../components/alert/alert'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}

function datify (unix_timestamp) {

    var date = new Date(unix_timestamp * 1000)
    var day = date.getDate()
    var month =  1 + date.getMonth()
    var year = date.getFullYear()
    var hour = ('0' + date.getHours()).slice(-2)
    var minute = ('0' + date.getMinutes()).slice(-2)
    var second = ('0' + date.getSeconds()).slice(-2)

    var formattedTime = day + '.' + month + '.' + year + " в " + hour + ":" +  minute + ":" + second
    return formattedTime
}

function Investor(props) {

    let [index, setIndex] = useState(0)
    let [is404, display404] = useState(false)
    let [headerText, setHeaderText] = useState("")
    let [alertText, setAlertText] = useState("")

    let token = localStorage.getItem('session-token')
    let loggedIn = false
    let id = -1

    if ((token != null) && (token != "null")) {
        //console.log("authorized with token =", token)
        id = localStorage.getItem('user-info')
        //console.log("id", id)
        loggedIn = true
    } else {
        //console.log("not authorized")
        //console.log("not authorized with token =", token)
    }

    let [userInfo, setUserInfo] = useState({})
    let [transactions, setTransactions] = useState({})

    useEffect(()=>{
        axios.get('https://api.stonks.xyz/api/v1/user/info/' + props.match.params.id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setUserInfo(response.data)
                //console.log(JSON.stringify(userInfo))
            })
            .catch(function (error) {
                setUserInfo({})
                display404(true)
                setHeaderText("Ошибка")
                setAlertText(error.message)
                //loadStonk(getStock(JSON.stringify(error.message)))
        })

        axios.get('https://api.stonks.xyz/api/v1/user/transactions/' + props.match.params.id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setTransactions(response.data)
                //console.log(JSON.stringify(userInfo))
            })
            .catch(function (error) {
                setTransactions({})
                display404(true)
                setHeaderText("Ошибка")
                setAlertText(error.message)
            //loadStonk(getStock(JSON.stringify(error.message)))
        })
    }, [])

    return ((headerText=="Ошибка")||(alertText!=""))? 
        <View>
            <Alert
                visible={true}
                header="Не удаётся отобразить пользователя"
                text={alertText}
                onClose={()=>window.history.back()}
            />
        </View>:
        <View backAction={()=>window.location.href="/investor"} title={(userInfo.user)? userInfo.user.name : headerText} showBack={true} header={(userInfo.user)? userInfo.user.name : headerText}>
            
            {((userInfo.user) && (id != 0)) ? <>
            <Divider/>
            
            <div className="view__left view__left--small">
            
            <Text text={"Стоимость активов: " + fin(userInfo.net_worth)}/>
            <Row name="Портфель акций" value={fin(userInfo.portfolio_price)}/>
                <Row name={(loggedIn && (props.match.params.id==id))?"Мой баланс":"Баланс пользователя"} value={fin(userInfo.user.balance)}/>
             
            {(loggedIn && (props.match.params.id==id)) && <>
               <Button onClick={()=>{localStorage.setItem('session-token', null); document.location.href="/"}} text="Выйти из аккаунта" color="#353DF6" backgroundColor="#D0EBFF"/>
                <Divider/>
            </>}
            <a style={{textDecoration: "none"}} href={"https://" + userInfo.user.platform + ".ru/u/" + userInfo.user.platform_id}><Row name={userInfo.user.platform + ".ru/u/" + userInfo.user.platform_id + " ->"}/></a>
            
            <Button text={"Поделиться"} onClick={() => {navigator.clipboard.writeText(document.location.href); alert("Адрес скопирован в буфер обмена")}} color="white" backgroundColor={"#000"}/>
            <Divider/>
            </div>

            <div className="view__left view__left--large">

            <Tabs tabs={["Портфель", "Транзакции"]} index={index} setIndex={setIndex}/>
            

            {(index==1) && <>

                { transactions.transactions.map((el) => 
                    <NavLink to={"/listing/" + el.stock.id}>
                        <StockOne image={el.stock.pic} name={(el.stock.ticker)? ((el.stock.ticker).toUpperCase()) : "ОШИБКА"} ticker={/*el.stock.name + " " + el.stock.platform*/datify(el.dateTime)} change={fin(el.price) + ", " + el.amount + " шт"} value={((el.kind == "buy")? "-":"+") + "" + fin(el.price*el.amount)} color={((el.kind == "sell")? -1:+1)}/>
                    </NavLink>
                )}
            </>
            }

            {(index==0) && <>
                { userInfo.user.stocks.map((el) => 
                    
                    <NavLink to={"/listing/" + el.stock.id}>
                        <StockOne image={el.stock.pic} name={(el.stock.ticker)? ((el.stock.ticker).toUpperCase()) : "ОШИБКА"} ticker={el.stock.name + " на " + el.stock.platform} value={el.amount + " шт"} color={0}/>
                    </NavLink>
                )}
            </>
            }
            </div>
            </> : <Loader/>}
            
        </View>
}

export default Investor;

  