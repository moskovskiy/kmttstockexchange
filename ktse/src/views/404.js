import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/bg.png'

function NoRoute(props) {
    console.log(JSON.stringify(props))
    return (
        <View title="Не найдено" header="404">
            <Text text="Нам нечего вам тут показать"/>
            <Row name={"stonks.xyz"+props.location.pathname} nowrap={true}/>
            
            <Button onClick={(e)=>{window.history.back()}} text="Вернуться" color="#E52046" backgroundColor="#FFEDED"/>
            
        </View>
    );
}

export default NoRoute;

  
