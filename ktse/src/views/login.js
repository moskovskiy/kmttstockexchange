import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React, { useState } from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Input from '../components/input/input'
import Text from '../components/text/text'
import Divider from '../components/divider/divider'
import Row from '../components/row/row'
import bg from '../media/bg.png'

function Login() {
    let [link, setLinkF] = useState ("")
    let [inkey, setKeyF] = useState ("")
    let [invalue, setValueF] = useState ("")
    let [danger, setDanger] = useState (false)
    let [tokened, setTokened] = useState (false)

    function setKey (e) {
        setKeyF(e.target.value)
    }

    function setValue (e) {
        setValueF(e.target.value)
    }

    function setLink (e) {
        setLinkF(e.target.value)
    }

    return (
        <View title="Войти в аккаунт" header="Добро пожаловать!">

            <div className="view__left">
                <Text text="Чтобы работать с биржей, отправьте ‘/auth’ одному из следующих ботов:"/>
                <Button onClick={()=>document.location.href="https://tjournal.ru/m/341235"} text="Боту TJournal" color="#000" backgroundColor="#FFE577"/>
                <Button onClick={()=>document.location.href="https://dtf.ru/m/284903"} text="Боту DTF" color="#000" backgroundColor="#D1EDFF"/>
                <Divider/>
            </div>

            <div className="view__left">
                <Text text="Есть ссылка вида stonks.xyz/auth/abc-def-ghi?"/>
                <Input onChange={setLink} value={link} header="Ссылка для входа" hint="https://stonks.xyz/auth/abcde-fghhi-jklmn"/>
                <Button text={"Войти по ссылке"} color="#fff" backgroundColor="#000" onClick={()=>{
                    document.location.href = link
                }}/>

                <Divider/>
            </div>

            <div className="view__left">
                <Button text={danger ? "Скрыть инструменты разработчика" : "Вход для поддержки"} color="#555555" backgroundColor="white" onClick={()=>setDanger(!danger)}/>
                {danger && <>
                    <Row nowrap={true} name="Не используйте эту форму, только если вас не попросила поддержка"/>
                    <br/>
                    <Row nowrap={true} name="Форма даёт низкоуровневый доступ, потому могут быть любые последствия"/>
                    <br/>
                    <Input type="password" onChange={setValue} value={invalue} header="Пользователь" hint="abcde-fghhi-jklmn"/>
                    <Input type="password" onChange={setKey} value={inkey} header="Сессия" hint="abcde-fghhi-jklmn"/>
                    <br/>
                    
                    <Button text={"Переустановить токены"} onClick={()=>{
                        alert("Проверьте ID пользователя, доступ к секьюрным данным осуществляется только по токену, чужой ID даёт имитацию доступа к аккаунту, однако все действия будут недоступны")
                        localStorage.setItem('user-info', invalue)
                        localStorage.setItem('session-token', inkey)
                        document.location.href="/"
                    }}/>

                    {tokened
                    ? <>
                        <br/>
                        <br/>
                        <Text text="Не называйте этот токен никому, даже поддержке"/>
                        <Row name={localStorage.getItem('session-token')}/>
                    </>
                    : <Button text={"Показать текущий токен"} color="#000" backgroundColor="#eee" onClick={()=>{
                        let r = window.confirm("Убедитесь, что кроме вас никто не сможет увидеть токен, он даёт полный доступ к вашему аккаунту. ПОДДЕРЖКА НИКОГДА НЕ ПОПРОСИТ ПРИСЛАТЬ ЭТОТ ТОКЕН СЕБЕ. Показать?");
                        if (r) {
                            setTokened(true)
                        }
                    }}/>}
                </>}
            </div>
        </View>
    );
}

export default Login;

  
