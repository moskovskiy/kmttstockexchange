import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import axios from 'axios'
import React, { useEffect, useState } from 'react'
import View from '../components/view/view'
import Text from '../components/text/text'
import Alert from '../components/alert/alert'

function Auth(props) {

    let [alertText, setText] = useState("")

    useEffect(()=>{

        function request() {
            axios.get('https://api.stonks.xyz/api/v1/user/auth/' + props.match.params.token).then((response) => {
                localStorage.setItem('session-token', response.data.session_token)
                localStorage.setItem('user-info', response.data.user.id)
                localStorage.setItem('zoom', 'week')
                window.location.href=("/investor/"+response.data.user.id)
            })
            .catch(function (error) {
                setText(JSON.stringify(error.message))
                console.log(error)
                localStorage.setItem('session-token', null)
                return
            })
        }

        if ((localStorage.getItem('session-token') != null)&&(localStorage.getItem('session-token') != "null")) {
            let r = window.confirm("Вы уже вошли ранее. Хотите войти по новой ссылке?");
            if (r) {
                request()
            } else {
                window.location.href=("/investor/"+localStorage.getItem('user-info'))
            }
        } else {
            request()
        }
    }, [])

    return (
        <View header="Идет переадресация">{(alertText=="")?
            
            <Text text="Если ничего не происходит, убедитесь в верости токена"/>
            :
            <Alert
                visible={true}
                header="Не удалось залогиниться"
                text={alertText + " (Попробуйте войти в аккаунт заново)"}
                action="Войти снова"
                onClose={()=>{window.location.href="/"}}
            />
        }</View>
    );
}

export default Auth;

  
