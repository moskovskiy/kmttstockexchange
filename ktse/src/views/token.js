import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/bg.png'

function Token() {
  
    return (
        <View title="НЕ ОТКРЫВАТЬ" header="Никому не показывайте и не называйте этот токен, даже поддержке">
            <Text text="Он выдаёт полный доступ в аккаунт"/>
            <Divider/>
            <Text text={localStorage.getItem('session-token')}/>    
            <Divider/> 
        </View>
    );
}

export default Token;

  
