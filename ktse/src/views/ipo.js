import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/ipo.png'

function Pro() {
  
    return (
        <View title="Выйдите на IPO" header="">
            
            <div className="view__left">
                <img src={bg} className="view__image"/>
                <Text text="Предложим при попадании в Топ-10 блогов или комментаторов за месяц на DTF или TJ"/>    
                <Divider/>
            </div> 
            <div className="view__left">
                <Text text="Вкладывайтесь в своё развитие!"/>
                <Row nowrap={true} name="Разместите публично акции своего блога – станьте акционером своего блога"/>
                <br/>
                <Row nowrap={true} name="• Привлечение дохода с продажи своих акций"/>
                <Row nowrap={true} name="• Сразу после IPO все ваши акции принадлежат вам"/>
                <Row nowrap={true} name="• Развиваете блог – растёт цена!"/>
                <Row nowrap={true} name="• Продавайте много или держите и растите цену!"/>
            </div>
        </View>
    );
}

export default Pro;

  
