import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"

import React, { useEffect, useState } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Stock from '../components/stock/stock'
import Loader from '../components/loader/loader'
import Alert from '../components/alert/alert'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    parts[1] = parts[1].slice(0,2)
    return parts.join(".");
}

function Landing(props) {

    let token = localStorage.getItem('session-token')
    let loggedIn = false
    let [alertText, setAlertText] = useState("")
  
    if ((token != null) && (token != 'null')) {
        //console.log("authorized")
        loggedIn = true
    } else {
        window.location.href="/listing"
    }

    let [requests, setRequests] = useState({})

    useEffect(() => {
        axios.get('https://api.stonks.xyz/api/v1/market/orders/my', {
            headers: { 'Authorization': token,
                'Content-Type' : 'text/plain' }
            })
        .then((response) => {
            setRequests(response.data.orders)
            //console.log(requests)
        })
        .catch(function (error) {
            setAlertText(error.message)
        })
        
        const interval = setInterval(() => {
            axios.get('https://api.stonks.xyz/api/v1/market/orders/my', {
                headers: { 'Authorization': token,
                    'Content-Type' : 'text/plain' }
                })
            .then((response) => {
                setRequests(response.data.orders)
                //console.log(requests)
            })
            .catch(function (error) {
                setAlertText(error.message)
            })

        }, 1000);

        /*const unlisten = window.history.listen(() => {
            window.scrollTo(0, 0);
        });*/
        return () => {
            clearInterval(interval);
            //unlisten();
        }

    }, [])

    function cancelRequestFor(id) {
        const options = {
            method: 'POST',
            headers: { 
                'content-type': 'application/json',
                'Authorization': token
            },
            data: {
                order_id: id,
            },
            url: 'https://api.stonks.xyz/api/v1/market/orders/cancel'
        };

        axios(options)
        .then((response) => {
            if (response.data.message == "ok") {
                console.log("Deleted")
            } else {
                setAlertText(JSON.stringify(response.data.message))
            }
            window.location.reload()
        })
        .catch(function (error) {
            setAlertText(error)
        })
    }
  
    return (
        <View title="Мои заявки" header={(Object.values(requests).length == 0)?"Пока пусто":"Мои заявки"}>
            {(alertText != "") && 
            <Alert
                visible={true}
                header="Ошибка в загрузке заявок"
                text={alertText}
                onClose={()=>setAlertText("")}
            />
            }
            {requests ? Object.values(requests).map((el)=><div className="view__left">
                <NavLink style={{textDecoration: "none"}} to={"/listing/"+el.stock.id}>
                    {el.stock && <Stock image={el.stock.pic} name={el.stock.ticker.toUpperCase()} ticker={el.stock.name + " на " + el.stock.platform}/>}
                </NavLink>
                
                <Row name="Тип" value={(el.type=="sell") ? "Продажа " : "Покупка "} color={0}/>
                <Row name="Количество" value={el.amount + " шт"} color={0}/>
                <Row name="Цена 1 шт" value={"¢" + fin(el.price)} color={0}/>
                <Row name="Номер заявки" value={el.id} color={0}/>
                <Row name="Статус" value={el.active? "На рынке" : "Завершена"}/>
                <Button onClick={(e)=>cancelRequestFor(el.id)} text="Отменить заявку" color="#E52046" backgroundColor="#FFEDED"/>
                <br/>
                <Divider/></div>
            ) : <Loader/>}

            {(Object.values(requests).length == 0) && <>
                <Text text="Если у вас есть заявки, они сейчас загрузятся"/>
            </>
            }
        </View>
    );
}

export default Landing;

  