import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/bg.png'

function About() {
  
    return (
        <View title="О бирже" header="Биржа TJ и DTF">
            <Text text="Что такое биржа?"/>
            <a href="https://www.notion.so/TJ-VC-DTF-dcf0d92627954c8db7c8f17b158ec4d3"><Row nowrap={true} name="Прочитайте подробный гайд"/></a>
            <Divider />
            <Text text="Как начать?"/>
            <Row nowrap={true} name="Чтобы начать работу с биржей, отправьте /auth в личные сообщения сообществу биржи на TJournal или DTF."/>
            <Divider />
            <Text text="Куда писать?"/>
            <Row nowrap={true} name="Roma Moskovskiy и QQ. Ищите нас на просторах TJ и DTF по всем вопросам."/>            
        </View>
    );
}

export default About;

  
