import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
} from "react-router-dom"

import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'
import React, { useState, useEffect } from 'react'

import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Tabs from '../components/tabs/tabs'
import Stock from '../components/stock/stock'
import Input from '../components/input/input'
import Loader from '../components/loader/loader'
import Alert from '../components/alert/alert'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}

function Sell(props) {

    let token = localStorage.getItem('session-token')
    let id = localStorage.getItem('user-info')

    let loggedIn = false
  
    if ((token != null) && (token != 'null')) {
        loggedIn = true
    } else {
        window.location.href="/"
    }
  
    let [index, setIndex] = useState(0)
    let [price, setPrice] = useState({})
    let [size, setSize] = useState({})
    let [success, setSuccess] = useState(0)
    let [alertText, setText] = useState("")

    function setPriceState (e) {
        setPrice(e.target.value)
    }

    function setSizeState (e) {
        setSize(e.target.value)
    }


    let [stonk, loadStonk] = useState({})
    let [prices, loadPrices] = useState({})
    let [portfolio, loadPortfolio] = useState({})
    let [displayError, setError] = useState({})
    let [glassPrices, setGlassPrices] = useState({})
    let [glass, setGlass] = useState({})

    function makeAlert (text) {
        setText(text)
    }

    useEffect(()=>{

        axios.get('https://api.stonks.xyz/api/v1/market/orders/book/' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setGlass(response.data)
            })
            .catch(function (error) {
        })

        axios.get('https://api.stonks.xyz/api/v1/stock/info/' + props.match.params.id, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                loadStonk(response.data)
            })
            .catch(function (error) {
                console.log(error)
        })

        axios.get('https://api.stonks.xyz/api/v1/user/check_stock/?user_id=' + id + '&stock_id=' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                loadPortfolio(response.data)
            })
            .catch(function (error) {
                console.log(error)
        })

        axios.defaults.adapter = require('axios/lib/adapters/http');
        axios.get('https://api.stonks.xyz/api/v1/market/stock/price/' + props.match.params.id, {responseType: 'stream', adapter: httpAdapter})
            .then((response) => {
                const stream = response;
                //console.log(stream)
                if (stream.data) {
                    loadPrices(stream.data)
                    /*stream.on('data', (chunk) => {
                        loadPrices(chunk)
                        console.log('UPDATE', chunk)
                    });
                    stream.on('end', () => {
                    
                    });*/
                }
        });


        const interval = setInterval(() => { 
            axios.get('https://api.stonks.xyz/api/v1/market/stock/price/' + props.match.params.id, {responseType: 'stream', adapter: httpAdapter})
            .then((response) => {
                const stream = response;
                //console.log(stream)
                if (stream.data) {
                    loadPrices(stream.data)
                    /*stream.on('data', (chunk) => {
                        loadPrices(chunk)
                        console.log('UPDATE', chunk)
                    });
                    stream.on('end', () => {
                    
                        });*/
                    }
            });   
        }, 1000);

        return () => {
            clearInterval(interval);
            //unlisten();
        }
    }, [])

    let stockPrice = Number(price)*size
    if (isNaN(stockPrice)) stockPrice = 0

    let text = ((index != 0)? "Продать" : "Купить") + (" за " + fin(stockPrice*((index==0)?1.005:1)))

    if (stockPrice == 69) text = "Nice"

    function requestBuy () {
        const options = {
            method: 'POST',
            headers: { 
                'content-type': 'application/json',
                'Authorization': token
            },
            data: {
                amount: size,
                price: price,
                type: ((index==0)?"buy":"sell"),
                stock_id: props.match.params.id,
                hours: 1,
                kind: "limit"
            },
            url: 'https://api.stonks.xyz/api/v1/market/orders/add'
        };

        axios(options)
        .then((response) => {
            if (response.data.message == "ok") {
                setSuccess(1)
                window.scrollTo(0, 0)
            } else {
                if (response.data.message == "not enough stocks in portfolio") {
                    makeAlert("В портфеле недостаточно акций для продажи")
                    return
                }

                if (response.data.message == "not enough money on the balance") {
                    makeAlert("На балансе недостаточно средств для покупки")
                    return
                }

                //makeAlert(JSON.stringify(response.data.message))
            }
        })
        .catch(function (error) {
            setError(error)
            setSuccess(2)
        })

        axios.get('https://api.stonks.xyz/api/v1/market/orders/book/' + props.match.params.id,{
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setGlass(response.data)
            })
            .catch(function (error) {
        })
    }
    
    //console.log(JSON.stringify(stonk))

    return (<>
        {(success == 0) && 
        <>

        <View title={(stonk.stock)? (["Покупка", "Продажа"][index] + " " + stonk.stock.ticker.toUpperCase()) : ""} showBack={true} header={(stonk.stock)? ("Новая заявка " + stonk.stock.ticker.toUpperCase()) : ""}>
            {(stonk.stock) ? <>
            

            <Alert
                visible={alertText!=""}
                header="Ошибка при создании заявки"
                text={alertText}
                onClose={()=>setText("")}
            />

            <div className="view__left">
            

            <Tabs tabs={["Покупка", "Продажа"]} index={index} setIndex={setIndex}/>
            

            {(index != 2) && <>
                
                {(index == 0) && stonk.stock &&<Row name="Доступно акций на рынке" value={JSON.stringify(stonk.volume) + " шт"} color={0}/>}
                {(index == 1) && <>{portfolio.portfolio ? <Row name="В портфеле" value={portfolio.portfolio.amount + " шт"}/> : <Text text="В портфеле нет таких акций!"/>}</>}
                {(index == 0) && <Row name="Последняя покупка" value={fin(prices.buy)} color={0}/>}
                {(index == 1) && <Row name="Последняя продажа" value={fin(prices.sell)} color={0}/>}
           
                <Divider/>

                <Input onChange={setSizeState} value={size} type="number" pattern="[0-9].*" header="Количество акций" hint="5"/>
                <Input onChange={setPriceState} value={price} type="number" pattern="[0-9].*" header="Стоимость 1 шт в ¢" hint={
                    ( (index == 1)
                        ? ((prices.sell)?(fin(prices.sell)):"¢50.00")
                        : ((prices.buy)?(fin(prices.buy)):"¢50.00")
                    )
                }/>
                {/*<Row name="Цена за 1 шт" value={"¢" + fin(price)}/>*/}
                
                {(loggedIn && (stockPrice > 0) && (price > 0)) ? <>
                    {(index == 0) && <Row name="Комиссия за покупку" value={fin(stockPrice*0.005)} color={0}/>}
                    <Button text={text} color="#fff" backgroundColor="#353DF6" onClick={()=>{
                        requestBuy()
                    }}/>
                </> : <>
                    <Button text={text} color="#fff" backgroundColor="#353DF6" onClick={()=>{
                        alert("Введите корректные данные")
                    }}/>
                </>}

                <Divider/>
                {/*<Button text={"Вернуться к акции"} onClick={()=>{
                        document.location.href=("/listing/"+props.match.params.id)
                }}/>*/}
            </>
            }

            </div>
            <div className="view__left">
            <>

                <Row name="Покупка-продажа происходит при появлении пары встречных заявок по одной и той же цене" nowrap={true}/>
                <Row name="Чтобы закрыть рыночную позицию встречной (по той же цене), нажмите на неё в стакане" nowrap={true}/>
                <Row name="Чтобы создать заявку по другой цене выберите действие" nowrap={true}/>
                <Divider/>
                <Row value="Цена" name="Продажа"/>
                {glass.sells && Object.keys(glass.sells).map(el=><>
                    <button className="none" onClick={()=>{
                        setIndex(0)
                        setPrice(el)
                        setSize(glass.sells[el])
                        window.scrollTo(0, 0)
                    }}>
                        <Row canhover={true} value={fin(el)} name={"" + glass.sells[el] + " шт"} color={-1}/>
                        <div style={{width: "100%", borderTop: "1px solid #D0EBFF"}}/>
                    </button>
                </>)}

                <Row value="Цена" name="Покупка"/>
                {glass.buys && Object.keys(glass.buys).map(el=><>
                    <button className="none" onClick={()=>{
                        setIndex(1)
                        setPrice(el)
                        setSize(glass.sells[el])
                        window.scrollTo(0, 0)
                    }}>
                    <Row canhover={true} value={fin(el)} name={"" + glass.buys[el] + " шт"} color={1}/>
                    <div style={{width: "100%", borderTop: "1px solid #D0EBFF"}}/>
                    </button>
                </>)}
            </>

            </div>
            
            </> : <Loader/>}
        </View></>}

        {(success == 1) && 

        <View header={((index==0)?"Покупка":"Продажа")+" размещена"}>

            <div className="view__left">
                <Button text={"Вернуться к акции"} onClick={()=>{
                            document.location.href=("/listing/"+props.match.params.id)
                }}/>
                
                <Button text={"Ещё заявка"} color="#353DF6" backgroundColor="#D0EBFF" onClick={()=>{
                        window.location.reload()
                }}/>
                <Divider/>
            </div>
            
            <div className="view__left">
                <Text text="Заявка"/>
                <Row name="Стоимость" value={fin(price*size)}/>
                <br/>

                <Row name="Акция" value={stonk.stock.ticker.toUpperCase()}/>
                <Row name="Цена" value={fin(price)}/>
                <Row name="Количество" value={size + " шт"}/>
            </div>
        </View>
        }

        {(success == 2) && 
            
            <View header="Операция отменена">
                <div className="view__left">
                <Text text="Произошла ошибка, заявка не может быть создана"/>
                <Row nowrap={true} name={"Описание ошибки: " + displayError}/>
                
                    <Button text={"Попробовать повторно"} color="#fff" backgroundColor="#353DF6" onClick={()=>{
                        setSuccess(0)
                        window.scrollTo(0, 0)
                    }}/>

                </div>
            </View>
        }

        {(success == 4) && 
            
            <View header="Биржа сейчас закрыта">
                <Text text="Биржа откроется в 8:00AM GMT+3"/>
            </View>
        }
    </>);
}

export default Sell;
