import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/pro.png'

function Pro() {
  
    return (
        <>
        <View invert={false} title="Stonks Pro" header="">
            
            <div className="view__left">
                <img src={bg} className="view__image"/>
                <Row nowrap={true} name="Будет доступно позднее"/>    
                <Divider/>
                <Text text="Получите ещё больше с подпиской PRO"/>
                <Row nowrap={true} name="• Пониженная комиссия"/>
                <Row nowrap={true} name="• Тёмная тема для торговли в любое время"/>
                <Row nowrap={true} name="• Уведомления о совершённых сделках"/>
                <Row nowrap={true} name="Список будет дополняться!"/>  
            </div> 
            <div className="view__left">
                <Button onClick={()=>{alert("Заходите попозже")}} text="Появится позднее"/>
            </div>
        </View>
        </>
    );
}

export default Pro;

  
