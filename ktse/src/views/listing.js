import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink
} from "react-router-dom"

import React, { useState, useEffect } from 'react'
import axios from 'axios'

import View from '../components/view/view'
import Alert from '../components/alert/alert'
import Text from '../components/text/text'
import Stock from '../components/stock/stock'
import Row from '../components/row/row'

import Loader from '../components/loader/loader'

function fin(x) {
    var parts = Number.parseFloat(x).toFixed(2).toString().split(".")
    if (isNaN(Number.parseFloat(x))) return "–"
    parts[0] = parts[0]? parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") : ""
    parts[1] = parts[1]? parts[1].slice(0,2) : ""
    return "¢"+parts.join(".")
}

function Listing(props) {

    let [lister, setList] = useState({})
    let [open, setOpen] = useState({})
    let [alertText, setText] = useState("")

    useEffect(() => {
        axios.get('https://api.stonks.xyz/api/v1/stocks/', {
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    setList(response.data)
                    //console.log(JSON.stringify(lister))
                })
                .catch(function (error) {
                    setText(JSON.stringify(error.message))
        })
        
        axios.get('https://api.stonks.xyz/api/v1/market/status', {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                setOpen(response.data.is_trading)
            })
            .catch(function (error) {
                setOpen(false)
        })

        const interval = setInterval(() => {
            axios.get('https://api.stonks.xyz/api/v1/market/status', {
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    setOpen(response.data.is_trading)
                })
                .catch(function (error) {
                    setOpen(false)
            })


            axios.get('https://api.stonks.xyz/api/v1/stocks/', {
                headers: { 'Authorization': localStorage.getItem('auth-token'),
                'Content-Type' : 'text/plain' }
                }).then((response) => {
                    setList(response.data)
                    //console.log(JSON.stringify(lister))
                })
                .catch(function (error) {
                    setText(JSON.stringify(error.message))
            })
        }, 3000);

        return () => {
            clearInterval(interval);
        }

    }, [])

    let currentdate = new Date(); 
    let datetime = 
                currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " "  
                + ("0" + currentdate.getHours()).slice(-2) + ":"   
                + ("0" + currentdate.getMinutes()).slice(-2) + ":"  
                + ("0" + currentdate.getSeconds()).slice(-2)
                
    return (
    (alertText!="") ? 
    <View title="" header="">
        <Alert
                visible={true}
                header="Не удаётся подключиться к бирже"
                text={alertText}
                onClose={()=>setText("")}
        />
    </View> :
    <View title="Все акции" header="">

        {(!localStorage.getItem('open')) ? <>
        <br/>
        <div className="view__header--alone">
                      <div style={{marginLeft: -20, fontSize: 20, marginTop: -50, fontWeight: 900}}>Торги сейчас недоступны</div>
                      <div style={{marginLeft: -20, marginTop: 10, fontSize: 12, fontWeight: 600}}>Биржа работает с 8:00 до 23:59 GMT+3</div>
        </div>
        <br/>
        <br/>
        <br/>
        </> : <>
        <div className="view__header--alone">
                        <div style={{marginLeft: -20, marginTop: -10, fontSize: 12, fontWeight: 600}}>
                            {/*datetime*/} 
                            <a style={{marginLeft: 20, color: "blue", fontSize: 16, textDecoration: "none"}} href="https://t.me/stonksxyz">
                              Заходите в телеграм чат! {"->"}
                            </a>
                        </div>
        </div>
        <br/>
        <br/>
        <br/>
        </>}
        {/*<Text text={datetime}/>*/}
        
        {(!lister.stocks) ? <Loader/> : Object.values(lister.stocks).map((el)=><>

            <div className="view__left">
            {el && <NavLink to={"/listing/"+el.id}>
                <Stock
                    image={el.pic}
                    name={el.ticker.toUpperCase()}
                    ticker={el.platform[0] + ": " + el.name.substring(0, 15)}
                    value={fin(lister.stock_prices[el.id])}
                    change={lister.diff_prices[el.id] ? lister.diff_prices[el.id] + "%" : "–"}
                color={lister.diff_prices[el.id]}/>
            </NavLink>}

            </div>
        </>)}
    </View>
);
}

export default Listing;

  
