import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import React from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import bg from '../media/bg.png'

function About() {
  
    return (
        <View title="Правила использования биржи" header="Правила использования биржи">
            <Text text="Так не стоит"/>
            <Row style={{height:100}} nowrap={true} name="Так как всем новым пользователям предоставляется денежный бонус, существует схема вывода баланса с одноразового аккаунта через подставную покупку акций по завышенной цене"/>
            <Divider/>
            <Text text="Мы бдим"/>
            <Row style={{height:100}} nowrap={true} name="Администрация биржи внимательно мониторит все действующие сделки и оставляет за собой право полностью отказать в обслуживании при выявлении случаев неправомерного получения средств"/>
            <Divider/>
            <Text text="И никакой ответственности"/>
            <Row style={{height:100}}  nowrap={true} name="Не является финансовым активом или инструментом. Авторы игры не несут ответственность за ваши действия, в том числе за ущерб, прямой или косвенный."/>
            <Divider/>
            <Text text="Ваша любимая биржа"/>
            <Row style={{height:100}}  nowrap={true} name="Перманентный бан не стоит риска. Зарабатывайте по правилам! Это приятно :)"/>
        </View>
    );
}

export default About;

  
