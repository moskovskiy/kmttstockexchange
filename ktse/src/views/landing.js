import {
BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom"

import './landing.css'

import React, { useState } from 'react'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Alert from '../components/alert/alert'

import bg from '../media/bg.png'
import wbg from '../media/wbg.png'

function Landing() {

    let [visible, setVisible] = useState(true)
    let token = localStorage.getItem('session-token')
    if ((token != null) && (token != "null")) {
        document.location.href="/listing"
    }
  
    return (
        <div className="landing__view__container">
            <img style={{backgroundImage: `url(${wbg})`}} className="landing__view__bg"/>
            <div className="landing__view">
                <img className="landing__bg" src={bg}/>
                <div className="landing__header">Покупайте акции популярнейших авторов на TJournal и DTF<br/> и зарабатывайте на росте стоимости</div>
                <div className="landing__text">Чтобы начать, отправьте ‘/auth’ одному из ботов:</div>
                <div className="landing__buttons">
                    <button onClick={()=>document.location.href="https://tjournal.ru/m/341235"} className="landing__login landing__login--tj">Боту TJ</button>
                    <button onClick={()=>document.location.href="https://dtf.ru/m/284903"} className="landing__login landing__login--dtf">Боту DTF</button>
                </div>
            </div>
            <View style={{pointerEvents:"none"}} title="Добро пожаловать" header=""/>
        </div>

    );
}

export default Landing;

  
