import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink
} from "react-router-dom"

import React, { useState } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Input from '../components/input/input'
import Stock from '../components/stock/stock'

function Investors() {
    let [addr, setAddrF] = useState()
    let [noInv, setNoInv] = useState(false)

    function setAddr (e) {
        setAddrF(e.target.value)
    }

    function getIdAndMove () {
        axios.get('https://api.stonks.xyz/api/v1/user/get_link/?link=' + addr, {
            headers: { 'Authorization': localStorage.getItem('auth-token'),
            'Content-Type' : 'text/plain' }
            }).then((response) => {
                if (response.data.user_id) {
                    window.location.href = "https://stonks.xyz/investor/"+response.data.user_id
                } else {
                    setNoInv(true)
                }
            })
            .catch(function (error) {
                setNoInv(true)
        })
    }

    //https://api.stonks.xyz/api/v1/user/get_link/?link=https://tjournal.ru/u/85142

    return (
    <View title="Найти инвестора" header="Найти инвестора">
        <div className="view__left">
            <Input onChange={setAddr} value={addr} header="Ссылка на TJ/DTF или ID" hint="https://dtf.ru/u/12345"/>
            <Button onClick={()=>getIdAndMove()} text="Найти по ссылке"/>
                <Button onClick={()=>window.location.href="/investor/"+addr} text="Найти по ID" color="#353DF6" backgroundColor="#D0EBFF"/>
            
            {noInv && <>
                <br/>
                <br/>
                <Text text="Не удалось найти пользователя в бирже"/>
                <br/>
            </>}
            <Divider/>
        </div>


        <div className="view__left">
            <Text text="Список Forbes"/>
                <a href="https://stonks.xyz/investor/1"><Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="TJournal.ru/u/85142"/></a>
                <Row name="Состояние" value="¢10 662 018.05" color={0}/>
                <Row name="Прибыль" value="+ ¢10 662 018.05" color={1}/>
                <br/>
        </div>
        {/*<div>
            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="DTF/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>

            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="DTF/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>

            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="TJournal/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>

            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="DTF/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>

            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="TJournal/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>

            <NavLink style={{textDecoration: "none"}} to="/investor/1">
                <Stock image="https://leonardo.osnova.io/a39dedab-5fe6-5602-91de-e5811b3aa909/-/scale_crop/300x300/-/format/png/" name="qq" ticker="DTF/123456"/>
                <Row name="Состояние" value="¢200.00" color={0}/>
                <Row name="Прибыль" value="+ ¢200.00" color={1}/>
                <br/>
            </NavLink>
        </div>*/}       
    </View>
);
}

export default Investors;

  
