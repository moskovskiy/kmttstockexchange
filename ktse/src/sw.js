const
	CACHE = "cache_static";


self.addEventListener("install", (e)=>{
	console.log(Accent("Installed"));

	function onInstall() {
		return caches.open(CACHE)
			.then(cache => cache.addAll([
				"/css/main.13ff572f.chunk.css",
			]));
	};

	e.waitUntil(onInstall(e));
});

self.addEventListener("activate", (e) =>{
	console.log(Accent("Activated"));
});



/**
 * @typedef {Object.<String>} FetchEvent~Event
 * @property {String} clientId
 * @property {Promise} preloadResponse
 * @property {Request.Request} request
 * @property {String} replacesClientId
 * @property {String} resultingClientId
 */

/**
 * **From Network** _with putting into Cache_
 * 
 * @param {Request} request
 */
function fromNetwork(request) {
	let putToCacheChecker = false,
		requestedURL = new URL(request.url);

	[
		/^\/manifest.json/g,
		/\.(woff2|woff|ttf|js|css)$/g,
	].forEach((iRegExp) => {
		if (iRegExp.test(requestedURL.pathname)) 
			putToCacheChecker = true;
	});

	if (putToCacheChecker)
		return new Promise((resolve, reject)=>{
			caches.open(CACHE).then((cache) =>
				fetch(request).then((response) =>
					cache.put(request, response).then(() =>
						fromCache(request).then((matching) => resolve(matching))
					)
				)
			)
		});
	else
		return fetch(request);
};

/**
 * **From Cache**
 * 
 * @param {Request} request
 */
function fromCache(request) {
	return caches.open(CACHE)
		.then((cache) => cache.match(request))
		.then((matching) => {
			if (matching)
				return matching;
			else
				return Promise.reject("no-match");
		});
};

self.addEventListener("fetch", /** @param {FetchEvent|Event} fetchEvent */ (fetchEvent) => {
	const { request } = fetchEvent;

	fetchEvent.respondWith(
		fromCache(request)
			.catch(() => fromNetwork(request))
	);
});